import os
import json
import shutil
from pathlib import Path
from integrity import IntegrityManager


# Declarative mod manager!
# It merges any amount of mods into one place through symlinks
# Load order is declarative: each mod depends on others
class ModMerger:

    def __init__(self, project_path):
        self.__project_path = Path(project_path)

        self.__definitions_file_path = os.path.join(
            self.__project_path,
            "definitions.json"
        )

        self.__install_log_file_path = os.path.join(
            self.__project_path,
            ".install_log.json"
        )

        self.__link_counter = 0

    def load_definitions(self):
        if not os.path.isfile(self.__definitions_file_path):
            print(f"New project created {self.__project_path}")
            self.__write_empty_definitions()

        with open(self.__definitions_file_path, "r") as file:
            self.__definitions = json.load(file)

            if "paths" not in self.__definitions:
                raise ValueError("Paths missing in your definition.json")

            if "backups_relative_path" not in self.__definitions["paths"]:
                raise ValueError("Backups relative path not set!")

            if "mods_relative_path" not in self.__definitions["paths"]:
                raise ValueError("Mods relative path not set!")

        excluded = self.__definitions["paths"]["excluded"]
        self.__im = IntegrityManager(self.__project_path, excluded)

        print(f"Calculate integrity of content: {self.__project_path} ...")
        self.__integrity = self.__im.get_integrity()

    def __load_install_log(self):
        if not os.path.isfile(self.__install_log_file_path):
            return {}

        with open(self.__install_log_file_path, "r") as install_log_file:
            install_log = json.load(install_log_file)
            return install_log

        raise ValueError("Cant load installation log!")

    def __save_install_log(self, install_log):
        with open(self.__install_log_file_path, "w") as install_log_file:
            json.dump(install_log, install_log_file, indent=2)

    def __write_empty_definitions(self):
        blank_definitions = {
            "paths": {
                "mods_relative_path": "umm_mods",
                "backups_relative_path": "umm_backups",
                "excluded": [
                    "prefix",
                    "wine",
                    "sandbox",
                    "hub",
                    "OAL",
                    "OpenModMan",
                    "appdata",
                    "GLCache",
                    "bin/GLCache",
                    "*.dxvk-cache",
                    "*.7z"
                ],
            },
            "mods": [
                {
                    "name": "blank_mod",
                    "relative_path": "blank_mod",
                    "enabled": False,
                    "description": "Blank mod which even does not exist.",
                    "version": "0.0.0.1",
                    "author": "acidpointer",
                    "release_url": "",
                    "source_url": "https://gitlab.com/acidpointer/umm",
                    "depends_on": [],
                }
            ]
        }

        print(blank_definitions["paths"]["excluded"])

        if blank_definitions["paths"]["excluded"] is None:
            blank_definitions["paths"]["excluded"] = []

        for p in ["mods_relative_path", "backups_relative_path"]:
            if blank_definitions["paths"][p] is not None:
                blank_definitions["paths"]["excluded"] \
                    .append(blank_definitions["paths"][p])

        with open(self.__definitions_file_path, "w") as definitions_file:
            json.dump(blank_definitions, definitions_file, indent=4)

    # TODO: Refactor and simplify
    def __build_load_sequence(self):
        if not isinstance(self.__definitions, dict) \
           or "mods" not in self.__definitions:
            raise ValueError("Wrong definitions format!")

        mods = self.__definitions.get("mods", [])

        if not isinstance(mods, list):
            raise ValueError("The 'mods' MUST be array!")

        mod_dict = {mod["name"]: mod for mod in mods}

        def is_mod_active(mod_name, visited, active_mods, as_dep=False):
            if mod_name in visited:
                return

            visited.add(mod_name)
            mod = mod_dict[mod_name]

            if mod["enabled"]:
                active_mods.add(mod_name)

                for dependency in mod.get("depends_on", []):
                    is_mod_active(dependency, visited, active_mods, True)
            else:
                if as_dep:
                    raise ValueError(
                        f"Disabled required mod: {mod_name}")

        visited = set()
        active_mods = set()

        for mod in mods:
            is_mod_active(mod["name"], visited, active_mods)

        mod_sequence = []

        def process_mod(mod_name, visited):
            if mod_name in visited:
                return
            visited.add(mod_name)

            deps = mod_dict[mod_name].get("depends_on", [])
            if len(deps) > 0:
                for dependency in deps:
                    process_mod(dependency, visited)
                    if mod_name not in mod_sequence:
                        mod_sequence.append(mod_name)
            else:
                process_mod(mod_name, visited)
                mod_sequence.append(mod_name)

        for mod in mods:
            if mod.get("enabled", False):
                process_mod(mod["name"], set())

        result = [mod_dict[mod_name] for mod_name in mod_sequence]

        return result

    def __link(self,
               src_dir,
               dest_dir,
               mod_name,
               install_log: dict,
               link_fn):
        for root, _, files in os.walk(src_dir):
            for file in files:
                src_file = os.path.join(root, file)
                rel_path = os.path.relpath(src_file, src_dir)
                dest_file = os.path.join(dest_dir, rel_path)

                dest_file_exist_link = os.path.islink(dest_file)
                if os.path.exists(dest_file) or dest_file_exist_link:
                    # Original files should be backuped!
                    # We move it to special backup directory
                    if rel_path in self.__integrity and \
                       not dest_file_exist_link:
                        if os.path.isfile(dest_file) and \
                           not os.path.islink(dest_file) and \
                           rel_path not in install_log:

                            back_file = os.path.join(
                                self.__backups_path,
                                rel_path
                            )

                            os.makedirs(
                                os.path.dirname(back_file),
                                exist_ok=True
                            )

                            shutil.move(dest_file, back_file)

                            if os.path.isfile(back_file):
                                print(f"File {rel_path} was backuped!")
                            else:
                                raise ValueError(
                                    f"Can't move file {rel_path} to backups!"
                                )
                    else:
                        # Complex thing. We update install_log
                        if rel_path in install_log:
                            mod_log: dict = install_log[rel_path]
                            if "overwriten_by_mods" in mod_log:
                                overwrites: dict = mod_log["overwriten_by_mods"]
                                if mod_name not in overwrites:
                                    if len(overwrites.keys()) > 0:
                                        overwrites[mod_name] = overwrites[
                                            next(reversed(overwrites.keys()))
                                        ] + 1
                                    else:
                                        overwrites[mod_name] = 1
                                    mod_log["overwriten_by_mods"] = overwrites
                            install_log[rel_path] = mod_log

                        # If file exist - we MUST delete it first!
                        if os.path.islink(dest_file):
                            os.unlink(dest_file)
                        else:
                            os.remove(dest_file)

                        # Stupid check but why not
                        if os.path.exists(dest_file):
                            raise ValueError(
                                f"Cant process mod {mod_name} \
                                because {rel_path} is not deleted"
                            )

                os.makedirs(os.path.dirname(dest_file), exist_ok=True)

                if link_fn:
                    link_fn(src_file, dest_file)
                # if use_symlinks:
                #     os.symlink(src_file, dest_file)
                # else:
                #     shutil.copy2(src_file, dest_file)

                if os.path.exists(dest_file):
                    self.__link_counter = self.__link_counter + 1
                    if rel_path not in install_log:
                        install_log[rel_path] = {
                            "origin_mod": mod_name,
                            "overwriten_by_mods": {}
                        }
                    continue

                raise ValueError(
                    f"File {dest_file} has not been created! MOD: {mod_name}")

    def __create_dirs_if_not_exist(self):
        self.__mods_path = os.path.join(
            self.__project_path,
            self.__definitions["paths"]["mods_relative_path"])

        # Create mods_path if not exist
        if not os.path.exists(self.__mods_path):
            os.makedirs(self.__mods_path, exist_ok=True)

        self.__backups_path = os.path.join(
            self.__project_path,
            self.__definitions["paths"]["backups_relative_path"]
        )

        if not os.path.exists(self.__backups_path):
            os.makedirs(self.__backups_path, exist_ok=True)

        # Create game_path if not exist
        if not os.path.exists(self.__project_path):
            os.makedirs(self.__project_path)

    def clean(self):
        install_logs = self.__load_install_log()
        for rel_path in install_logs.keys():
            abs_path = os.path.join(self.__project_path, rel_path)
            if os.path.exists(abs_path):
                os.remove(abs_path)

    # Move contents
    def merge(self):
        mod_sequence = self.__build_load_sequence()

        done_mods = set()

        self.__create_dirs_if_not_exist()

        install_log = self.__load_install_log()

        # core merge function
        def link_with_symlinks(src, dest):
            os.symlink(src, dest)

        for mod in mod_sequence:
            mod_name = mod["name"]
            mod_author = mod["author"]
            mod_version = mod["version"]
            mod_relative_path = mod["relative_path"]

            mod_source_dir = os.path.join(self.__mods_path, mod_relative_path)
            mod_destination_dir = self.__project_path

            if mod_name not in done_mods:
                print(f"Processing mod '{mod_name}'  ...")
                self.__link(
                    mod_source_dir,
                    mod_destination_dir,
                    mod_name,
                    install_log,
                    link_fn=link_with_symlinks
                )

                done_mods.add(mod["name"])

        self.__save_install_log(install_log)

        print(f"Installed {len(mod_sequence)} mods!")
        print(f"Total installed files: {self.__link_counter}")
