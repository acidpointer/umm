#!/usr/bin/env python3

import os
import sys
from merger import ModMerger


def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))


def main():
    path = get_script_path()

    mm = ModMerger(path)

    mm.load_definitions()
    mm.clean()
    mm.merge()


if __name__ == "__main__":
    main()
