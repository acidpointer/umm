from pathlib import Path
import os
import fnmatch
import json
import collections.abc
from imohash import hashfile


class IntegrityManager:

    def __init__(self, project_path, excluded_paths=[]):
        self.__project_path = Path(project_path)

        self.__integrity_file_path = os.path.join(
            self.__project_path,
            ".integrity.json"
        )

        # Before start doing something, simple validation is required!
        if isinstance(excluded_paths, collections.abc.Sequence):
            self.__excluded_paths = excluded_paths
            return

        raise ValueError("Excluded relative paths should be array!")

    def __is_excluded(self, relative_path):
        for ex_path in self.__excluded_paths:
            if fnmatch.fnmatch(relative_path, ex_path) or \
               relative_path == ex_path or \
               relative_path.startswith(ex_path + os.path.sep):
                return True
        return False

    def __create_directory_integrity(self, directory):
        # We can handle only directories
        if not os.path.isdir(directory):
            raise ValueError(f"Path {directory} should be directory!")

        def handle_integrity(file, file_path):
            if os.path.exists(file_path) \
                   and os.access(file_path, os.R_OK) \
                   and not self.__is_excluded(relative_path) \
                   and not os.path.islink(file_path):

                # WARNING! This hashing is insecure but fast
                # Should be enough for games

                print(file_path)
                hash = hashfile(file_path, hexdigest=True)
                return {
                    "file": file,
                    "hash": hash,
                }
            return None

        integrity = {}
        for root, _, files in os.walk(directory):
            for file in files:
                file_path = os.path.join(root, file)
                relative_path = os.path.relpath(file_path, directory)

                result = handle_integrity(
                    file,
                    file_path)

                if result is not None:
                    integrity[relative_path] = result

        return integrity

    def get_integrity(self, force=False):
        if not os.path.isfile(self.__integrity_file_path) or force:
            print("Root project integrity file not found! Creating new...")
            integrity = self.__create_directory_integrity(self.__project_path)

            with open(self.__integrity_file_path, "w") as integrity_file:
                json.dump(integrity, integrity_file, indent=2)
            return integrity
        with open(self.__integrity_file_path, "r") as integrity_file:
            return json.load(integrity_file)
